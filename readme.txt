=== About Backup updated files ===

This module will be used for taking backup of files. User can give start date and end date for backup.

User need to specify source folder from where backup need to take.
User need to specify destination folder from where backup files need to place.
User can see the previous backup files with the different details and he can download and delete the files.

There will be there link as defined below:

admin/settings/file_backup: Form to take backup.
admin/settings/file_backup/download: List of all previous backup files.
settings/file_backup/settings: Set the destination folder.

=== Dependencies ===
Module: date_popup

We will going to add more feature on it in near furture.

=== About the Authors ===
Yogesh Saini
http://drupal.org/user/725442
